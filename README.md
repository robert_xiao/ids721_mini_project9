# IDS721_mini_project9

## Overview:
Streamlit, known for its simplicity and efficiency, enables the creation of web apps for machine learning and data analysis with minimal coding, making it an attractive choice for quick deployment. By incorporating a model from Hugging Face, a platform that hosts a vast repository of pre-trained models covering a wide array of natural language processing (NLP) tasks, developers can leverage cutting-edge AI capabilities without the need for extensive computational resources or deep expertise in model training.<br>
## Requirements
- Create a website using Streamlit.<br>
- Connect to an open source LLM (Hugging Face).<br>
- Deploy model via Streamlit or other service (accessible via browser).<br>

## Steps:
1.	Write the code locally as follows:<br>
 ![Alt text](image.png)<br>
The functionality of the above codes is that we can input a text, and the app can output the emotional tendency of the text, whether it is positive or negative, and output the score.<br>
2.	Use the following code to create a website using Streamlit.<br>
 ![Alt text](image-1.png)<br>
3.	Use the following code to connect Hugging Face LLM model.<br>
 ![Alt text](image-2.png)<br>
4.	Use the following command line code to run locally the above codes.<br>
 ![Alt text](image-3.png)<br>
The website looks like the following.<br>
 ![Alt text](image-4.png)<br>
5.	We can test the functionality of the app, and use the following texts to test the website app, and the results are shown below.<br>
 ![Alt text](image-5.png)<br>
 ![Alt text](image-6.png)<br>

6.	We can use Streamlit Sharing to deploy our website app like the following.<br>
![Alt text](image-7.png)<br>
![Alt text](image-8.png)<br>